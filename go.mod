// +heroku install ./cmd/quaridirc/...
module gitlab.com/unerror/quarid-go

go 1.12

require (
	github.com/BurntSushi/toml v0.3.0 // indirect
	github.com/Sirupsen/logrus v1.0.5
	github.com/UnnoTed/fileb0x v0.0.0-20180423201250-180ae9f42c7f // indirect
	github.com/bmatcuk/doublestar v1.0.9 // indirect
	github.com/dlclark/regexp2 v1.1.6 // indirect
	github.com/dop251/goja v0.0.0-20180304123926-9183045acc25
	github.com/enmand/go-ircclient v0.0.0-20180227222740-467b0a4acb0e
	github.com/enmand/quarid-go v0.0.0-20180513175420-e43d73ef7cf4
	github.com/fsnotify/fsnotify v1.4.7 // indirect
	github.com/go-sourcemap/sourcemap v2.1.2+incompatible // indirect
	github.com/hashicorp/hcl v0.0.0-20180404174102-ef8a98b0bbce // indirect
	github.com/karrick/godirwalk v1.7.0 // indirect
	github.com/kr/pretty v0.1.0
	github.com/labstack/echo v3.3.5+incompatible // indirect
	github.com/labstack/gommon v0.0.0-20180426014445-588f4e8bddc6 // indirect
	github.com/magiconair/properties v1.7.6 // indirect
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.3 // indirect
	github.com/mitchellh/hashstructure v0.0.0-20170609045927-2bca23e0e452
	github.com/mitchellh/mapstructure v0.0.0-20180220230111-00c29f56e238 // indirect
	github.com/onsi/gomega v0.0.0-20180417102829-b7d1a52235cc
	github.com/pelletier/go-toml v1.1.0 // indirect
	github.com/pkg/errors v0.8.0
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/afero v1.1.0 // indirect
	github.com/spf13/cast v1.2.0 // indirect
	github.com/spf13/jwalterweatherman v0.0.0-20180109140146-7c0cea34c8ec // indirect
	github.com/spf13/pflag v1.0.1
	github.com/spf13/viper v1.0.2
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v0.0.0-20170224212429-dcecefd839c4 // indirect
	github.com/vbauerster/mpb v3.2.0+incompatible // indirect
	golang.org/x/crypto v0.0.0-20180503215945-1f94bef427e3 // indirect
	golang.org/x/net v0.0.0-20180502164142-640f4622ab69
	golang.org/x/sys v0.0.0-20180504064212-6f686a352de6 // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/yaml.v2 v2.2.1 // indirect
)
